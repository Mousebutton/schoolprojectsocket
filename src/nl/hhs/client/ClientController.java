package nl.hhs.client;

import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import nl.hhs.connection.Connector;
import nl.hhs.connection.MessageFactory;
import nl.hhs.connection.MessageParser;
import nl.hhs.server.Planning;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

@SuppressWarnings("Duplicates")
public class ClientController {
    private Connector connector;

    @FXML
    private TextArea area;
    @FXML
    private TextField txtInput;
    @FXML
    private TableView tblvCurtains;
    @FXML
    private Button btnNewPlanning;
    @FXML
    private Button btnSendValue;
    @FXML
    private ChoiceBox drpValues;

    private ObservableList<Planning> planningObservableList;

    @FXML
    protected void initialize()
    {
        planningObservableList = FXCollections.observableArrayList();
        TableColumn tblC = (TableColumn) tblvCurtains.getColumns().get(0);
        tblC.setCellValueFactory(   new PropertyValueFactory<>("planId")    );
        ((TableColumn) tblvCurtains.getColumns().get(1)).setCellValueFactory(new PropertyValueFactory<>("gordijn"));
        ((TableColumn) tblvCurtains.getColumns().get(2)).setCellValueFactory(new PropertyValueFactory<>("gordijnStand"));
        ((TableColumn) tblvCurtains.getColumns().get(3)).setCellValueFactory(new Callback<TableColumn.CellDataFeatures, ObservableValue>() {
            @Override
            public ObservableValue call(TableColumn.CellDataFeatures param) {
                return new ReadOnlyStringWrapper(Arrays.toString(((Planning)param.getValue()).getDag()));
            }
        });
        ((TableColumn) tblvCurtains.getColumns().get(4)).setCellValueFactory(new PropertyValueFactory<>("tijd"));
        ((TableColumn) tblvCurtains.getColumns().get(5)).setCellValueFactory(new PropertyValueFactory<>("dn"));
//        ((TableColumn) tblvCurtains.getColumns().get(6)).setCellValueFactory(new PropertyValueFactory<>("gordijn"));

        tblvCurtains.setItems(planningObservableList);

        drpValues.setItems(FXCollections.observableArrayList("0","10","20","30","40","50","60","70","80","90","100"));
        drpValues.setValue("0");
        btnNewPlanning.setOnAction( event -> {
            createPlanningPopup(0  );
        });
        btnSendValue.setOnAction( event -> {
            sendDataWithNoResponse(MessageFactory.createSetCurtainMessage(1,1,Integer.parseInt((String)drpValues.getValue()),"a",false));
        });
    }

    private void sendDataWithNoResponse(String msg)
    {
        Connector conn = null;
        try {
            conn = new Connector(Client.SERVERLOCATION,Client.SERVERPORT);

            while(true)
            {
                if(conn.hasMsg())
                {
                    MessageParser messageParser = new MessageParser(conn.getData());
                    if("getClientType".equalsIgnoreCase(messageParser.getCommand()))
                    {
                        System.out.println("Request ClientTest Type");
                        conn.sendData("#ClientType@ct:client|id:1$");
                    }
                    break;
                }
            }

            conn.sendData(msg);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            if(conn != null)
                conn.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private String sendDataWithReturnMessage(String msg)
    {
        Connector conn = null;
        try {
            conn = new Connector(Client.SERVERLOCATION,Client.SERVERPORT);

            while(true)
            {
                if(conn.hasMsg())
                {
                    MessageParser messageParser = new MessageParser(conn.getData());
                    if("getClientType".equalsIgnoreCase(messageParser.getCommand()))
                    {
                        System.out.println("Request ClientTest Type");
                        conn.sendData("#ClientType@ct:client|id:1$");
                    }
                    break;
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        String returnMessage;
        conn.sendData(msg);
        while(true)
        {
            if(conn.hasMsg()) {
                returnMessage = conn.getData();
                break;
            }
        }
        try {
            if(conn != null)
                conn.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return returnMessage;
    }
    private void createPlanningPopup(int id)
    {
        String days[] = new String[] {
                "Maandag",
                "Dinsdag",
                "Woensdag",
                "Donderdag",
                "Vrijdag",
                "Zaterdag",
                "Zondag"
        };
        final Stage dialog = new Stage();
        dialog.initModality(Modality.APPLICATION_MODAL);
        dialog.initOwner(btnNewPlanning.getScene().getWindow());

        List<CheckBox> lCheckboxes = Stream.generate(CheckBox::new)
                                           .limit(7)
                                           .collect(Collectors.toList());
        GridPane gpDays = new GridPane();

        for(int i = 0; i < lCheckboxes.size();i++)
        {
            gpDays.add(lCheckboxes.get(i),0,i);
            Label lblDay = new Label(days[i]);
            gpDays.add(lblDay,1,i);
            GridPane.setMargin(lblDay,new Insets(5));
        }

        ObservableList<Integer> lHours = FXCollections.observableArrayList(IntStream.range(0,23).boxed().collect(Collectors.toList()));
        ObservableList<Integer> lMinutes = FXCollections.observableArrayList(IntStream.range(0,60).boxed().collect(Collectors.toList()));

        ChoiceBox chbHours = new ChoiceBox(lHours);
        ChoiceBox chbMinutes = new ChoiceBox(lMinutes);

        chbHours.setValue(lHours.get(0));
        chbMinutes.setValue(lHours.get(0));

        Label lblHours = new Label("Uur:Minuut");

        GridPane gpHours = new GridPane();
        gpHours.add(lblHours,0,0);
        gpHours.add(chbHours,0,1);
        gpHours.add(chbMinutes,1,1);

        gpDays.add(gpHours,2,2);

        Button btnSavePlanning = new Button("Opslaan");

        gpDays.add(btnSavePlanning,4,7);

        btnSavePlanning.setOnAction((ActionEvent event) -> {

            int i = 0;
            List<Integer> lDays = new ArrayList<>();
            for (Node node : gpDays.getChildren().filtered((a) -> a instanceof CheckBox)) {
                i++;

                if(((CheckBox)node).isSelected())
                {
                    lDays.add(i);
                }
            }
            String hours = chbHours.getValue()+"-"+chbMinutes.getValue();
            int pId = planningObservableList.size() + 1;
            Planning p = new Planning(1, lDays.stream().mapToInt((a)->a).toArray(), hours, 1, 100, pId, "Gordijn 1", false);
            planningObservableList.add(p);

            sendDataWithNoResponse(MessageFactory.createSetPlanningMessage(p));
        });

        Scene dialogScene = new Scene(gpDays, 400, 400);
        dialog.setScene(dialogScene);
        dialog.show();
    }
    @SuppressWarnings("Duplicates")
    @FXML
    private void handleSendButton(ActionEvent event) {
        planningObservableList.remove(0,planningObservableList.size());
        Planning p = new Planning(1,new int[]{1,2},"15-15",1,50,1,"a",true);
        String msg = MessageFactory.createSetPlanningMessage(p);
        System.out.println(msg);

        String stringAllPlannings = this.sendDataWithReturnMessage(MessageFactory.createAllPlanning("1"));

        List<Planning> lPlannings = new ArrayList<>();

        MessageParser mp = new MessageParser(stringAllPlannings);

        if(!mp.getArgs().isEmpty())
        {
            for (HashMap<String, String> keyValue: mp.getArgs()) {
                lPlannings.add(new Planning(Integer.parseInt(keyValue.get("p")),
                                            Arrays.stream(keyValue.get("d").split(",")).mapToInt(Integer::parseInt).toArray(),
                                            keyValue.get("t"),
                                            Integer.parseInt(keyValue.get("g")),
                                            Integer.parseInt(keyValue.get("v")),
                                            Integer.parseInt(keyValue.get("c")),
                                            keyValue.get("n"),
                                            Boolean.parseBoolean(keyValue.get("dn"))));
            }
        }

        planningObservableList.addAll(lPlannings);

    }
    private void addLineToConsole(String text)
    {
        this.area.appendText(text+System.lineSeparator());
    }
}
