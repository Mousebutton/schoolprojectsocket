package nl.hhs.client;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import nl.hhs.connection.Connector;
import nl.hhs.server.Server;

import java.io.IOException;
import java.io.PrintWriter;

public class Client extends Application {
    private Connector connection;
    private PrintWriter output;
    public static final String SERVERLOCATION = "192.168.43.8";
  //  public static final String SERVERLOCATION = "145.107.163.130";
    public static final int SERVERPORT = Server.serverPort;

    @Override
    public void start(Stage primaryStage) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("Client.fxml"));
        primaryStage.setResizable(false);
        primaryStage.setTitle("ClientTest");
        primaryStage.setScene(new Scene(root,900,500));
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

    public Client() {
    }
}
