package nl.hhs.server;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.Scanner;

public class log {

	private static String notlogged = "";

	public log() {

	}

	static public synchronized void write(String bericht) {
		String date ="["+ Calendar.getInstance().getTime().toString()+"]";
		// Calendar.DAY_OF_MONTH + "-" + Calendar.MONTH + "-" + Calendar.YEAR;
		File bestand = new File("log.txt");
		String schrijf;
		PrintWriter tekst;
		Scanner geschrevenbestand;
		String geschreven = "";

		if( notlogged.length() != 0 ){
			bericht = notlogged +System.lineSeparator()+ bericht;
		}
		// bericht = notLogged + bericht;

		try {
			tekst = new PrintWriter(bestand);
			geschrevenbestand = new Scanner(bestand);
			while(geschrevenbestand.hasNextLine()){
				geschreven = geschreven + geschrevenbestand.nextLine() + System.lineSeparator();
			}
			schrijf = geschreven + date + bericht+ System.lineSeparator();
			tekst.println(schrijf);
			tekst.flush();
			tekst.close();
			geschrevenbestand.close();
			notlogged = "";
		} catch (FileNotFoundException e) {
			// add to notlogged
			notlogged = bericht;
			e.printStackTrace();
		}

	}

}
