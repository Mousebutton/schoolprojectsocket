package nl.hhs.server;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class Server extends Application {

//    public static final String serverLocation = "172.20.10.4";
	public static final String serverLocation = "localhost";
    public static final int serverPort = 8010;

    @Override
    public void start(Stage primaryStage) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("Server.fxml"));
        primaryStage.setResizable(false);
        primaryStage.setTitle("Server");
        primaryStage.setScene(new Scene(root,900,500));
        primaryStage.show();
    }

    public static void main(String args[])
    {
        launch(args);
    }
}
