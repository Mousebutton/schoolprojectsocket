package nl.hhs.server;

import nl.hhs.connection.ConnectionStatus;
import nl.hhs.connection.Connector;
import nl.hhs.connection.MessageParser;

import java.io.IOException;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

public class ClientHandler extends Thread {
    public enum ClientType
    {
        central,
        client
    }
    
    ClientType connectedType;
    int connectedId;

    public static Map<ClientType, Map<Integer, Connector>> clients = new HashMap<>();
    static {
        clients.put(ClientType.central, new HashMap<>());
        clients.put(ClientType.client, new HashMap<>());
    }

    /*
     *  Centrale -> [1, Connector]
     *           -> [2, Connector]
     *  ClientTest   -> [1, Connector]
     */
    private Connector connection;

    public ClientHandler(Socket serverSocket) throws IOException {
        connection = new Connector(serverSocket);
        System.out.println(connection.connection);
        connection.sendData("#getClientType@$");

        MessageParser messageParser;
        while(true)
        {
            if(this.connection.hasMsg())
            {
                messageParser = new MessageParser(this.connection.getData());
                break;
            }
        }

        if("ClientType".equals(messageParser.getCommand()))
        {
            System.out.println("Request ClientTest Type");
            if("central".equals(messageParser.getArgs().get(0).get("ct")))
            {
            	connectedType = ClientType.central;
              clients.get(ClientType.central).put(Integer.parseInt(messageParser.getArgs().get(0).get("id")),this.connection);
            } else {
            	connectedType = ClientType.client;
                clients.get(ClientType.client).put(Integer.parseInt(messageParser.getArgs().get(0).get("id")),this.connection);
            }
            connectedId = Integer.parseInt(messageParser.getArgs().get(0).get("id"));
            System.out.println(connectedId);
        }
    }

    @Override
    public void run() {
        System.out.println("Client " + connectedId + " connected");

		String msg;
		while (!Thread.currentThread().isInterrupted()) {
			if (this.connection.hasMsg()) {
				msg = this.connection.getData();
				System.out.println(Thread.currentThread().getId() + "-" + msg);
				MessageParser s = new MessageParser(msg);
				int client;

				// Pinging hey
				if (msg.equals("getHey")) {
					if (clients.get(ClientType.central).containsKey(connectedId)) {
						clients.get(ClientType.central).get(connectedId).sendData(ConnectionStatus.ISOPEN.toString());
						// System.out.println(clients.get(ClientType.central).get(connectedId));
					} else {
						break;
					}
				}
				// close connection
				if (msg.equals(ConnectionStatus.CLOSE.toString())) {
					break;
				}
				
				if ("getCurtain".equals(s.getCommand())) {
					client = Integer.parseInt(s.getArgs().get(0).get("c"));
					Connector centrale = clients.get(ClientType.central).get(client);
					centrale.sendData("#getAllCurtains@$");
					// long timeout = 0;
					while (true) {
						if (centrale.hasMsg()) {
							MessageParser data = new MessageParser(centrale.getData());
							data.getArgs().forEach(
									(a) -> a.forEach((b, c) -> System.out.println("Key:" + b + " Value:" + c)));
							break;
						}
					}
				} else if ("open".equals(s.getCommand())) {
					this.connection.sendData("open");
				} else if ("setCurtain".equals(s.getCommand())) {
					client = Integer.parseInt(s.getArgs().get(0).get("c"));
					Connector centrale = clients.get(ClientType.central).get(client);
					centrale.sendData(msg);
					// long timeout = 0;
					while (true) {
						if (centrale.hasMsg()) {
							MessageParser data = new MessageParser(centrale.getData());
							break;
						}
					}
					// deletePlanning getAllPlanning setPlanning getSensorData
				} else if ("deletePlanning".equals(s.getCommand())) {
					int id = Integer.parseInt(s.getArgs().get(0).get("p"));
					PlanningThread.delete(id);
				} else if ("getAllPlanning".equals(s.getCommand())) {
					String data = PlanningThread.getAllPlanning(1);
					this.connection.sendData(data);
				}else if ("setPlanning".equals(s.getCommand())){
					PlanningThread.setPlanning(msg);
				}else{
					System.out.println("not available");
				}
			}

            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if(!ServerController.running){
            	break;
            }
        }

        try {
            this.connection.close();
            clients.remove(connectedType).remove(connectedId);
            System.out.println("thread closed and "+connectedId+" removed");
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Disconnecting");
    }
}
