package nl.hhs.server;


import java.util.Arrays;

public class Planning {

	private int centraleId;
	private int[] dag;
	private String tijd;
	private int gordijn;
	private int planId;
	private int gordijnStand;
	private String naam;
	private boolean dn;
	
	public Planning(int centraleId, int[] dag, String tijd, int gordijn, int value, int id, String naam, boolean dn){
		this.setCentraleId(centraleId);
		this.setDag(dag);
		this.setTijd(tijd);
		this.setGordijn(gordijn);
		this.setGordijnStand(value);
		this.setPlanId(id);
		this.setNaam(naam);
		this.setDn(dn);
	}

	@Override
	public String toString() {
		return this.getCentraleId() +"-"+ Arrays.toString(this.getDag())+"-"+ this.getTijd() +"-"+ this.getGordijn() +"-"+ this.getPlanId() +"-"+ this.getGordijn() +"-"+ this.getNaam() +"-"+ this.isDn();
	}

	public int getCentraleId() {
		return centraleId;
	}

	public void setCentraleId(int centraleId) {
		this.centraleId = centraleId;
	}

	public int[] getDag() {
		return dag;
	}

	public void setDag(int[] dag) {
		this.dag = dag;
	}

	public String getTijd() {
		return tijd;
	}

	public void setTijd(String tijd) {
		this.tijd = tijd;
	}

	public int getGordijn() {
		return gordijn;
	}

	public void setGordijn(int gordijn) {
		this.gordijn = gordijn;
	}

	public int getPlanId() {
		return planId;
	}

	public void setPlanId(int planId) {
		this.planId = planId;
	}

	public int getGordijnStand() {
		return gordijnStand;
	}

	public void setGordijnStand(int gordijnStand) {
		this.gordijnStand = gordijnStand;
	}

	public String getNaam() {
		return naam;
	}

	public void setNaam(String naam) {
		this.naam = naam;
	}

	public boolean isDn() {
		return dn;
	}

	public void setDn(boolean dn) {
		this.dn = dn;
	}
}
