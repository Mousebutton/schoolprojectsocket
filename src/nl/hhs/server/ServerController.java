package nl.hhs.server;

import javafx.fxml.FXML;
import javafx.scene.control.TextArea;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;

public class ServerController {
	public static Boolean running = true;
	private ServerSocket serverSocket;
	private Socket clientSocket;
	
    private Thread serverThread = null;
    @FXML
    private TextArea txtConsole;
    

    @FXML
    private void onbtnServerStartClick()
    {
    	// planning thread
    	PlanningThread test = new PlanningThread();
		Thread t = new Thread(test);
		t.start();
		//
    	this.startSocketServer();
        this.addLineToConsole("Socketserver started");
        System.out.println("server started");
        
        running = true;
    }
    @FXML
    private void onbtnServerStopClick()
    {
        this.stopServer();
        this.addLineToConsole("Socketserver stopped");
    }
    private void addLineToConsole(String msg)
    {
        this.txtConsole.appendText(msg+System.lineSeparator());
    }
    private void startSocketServer()
    {
        if(this.serverThread == null)
        {
            this.serverThread = new Thread(() -> {
               
                try {
                    serverSocket = new ServerSocket(Server.serverPort);
                    serverSocket.setSoTimeout(5000);
                } catch (IOException e) {
                    e.printStackTrace();
                    System.exit(-1);
                }
                while(!Thread.currentThread().isInterrupted())
                {
                    try {
                        clientSocket = serverSocket.accept();
                        System.out.println("new connection");
                        ClientHandler client = new ClientHandler(clientSocket);
                        client.start();
                    } catch (SocketTimeoutException e) {
                        System.out.println("No new connection made");
                    }
                    catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                System.out.println("Server stopped.");
            });
            this.serverThread.start();
        }
    }
    private void stopServer()
    {
    	//ClientHandler.
    	running = false;
    	
        this.serverThread.interrupt();
        clientSocket = null;
        serverSocket = null;
        serverThread = null;
    }
}
