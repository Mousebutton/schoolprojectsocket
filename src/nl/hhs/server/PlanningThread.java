package nl.hhs.server;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import nl.hhs.connection.MessageFactory;
import nl.hhs.connection.MessageParser;
import nl.hhs.server.ClientHandler.ClientType;


public class PlanningThread implements Runnable {

	private static ArrayList<Planning> planningen = new ArrayList<Planning>();
	private static int idcreator = 1;
	private static final Lock editLock = new ReentrantLock();
	private static final Condition editCon = editLock.newCondition();
	
	
	
	public static boolean exist(int id) {
		try {
			editLock.lock();
			// locked so no outofbounds
			for (int i = 0; i < planningen.size(); i++) {
				if (planningen.get(i).getPlanId() == id) {
					return true;
				}
			}
		} finally {
			editLock.unlock();
		}
		return false;
	}

	// #setPlanning@c:1|u:ali|g:1|d:1,2,6,7|t:14-10|v:80$
	public static void setPlanning(String msg) {
		// lock
		editLock.lock();
		MessageParser line = new MessageParser(msg);
		ArrayList<HashMap<String, String>> arg = line.getArgs();

		for (HashMap<String, String> stringStringHashMap : arg) {
			stringStringHashMap.forEach((k,v)-> System.out.println("k:"+k+"-"+v));
		}

		int centraleId = Integer.parseInt(arg.get(0).get("c"));
		int gordijn = Integer.parseInt(arg.get(0).get("g"));
		int[] dag =  Arrays.asList(arg.get(0).get("d").replaceAll("\\[","").replaceAll("]","").replaceAll(" ","").split(",")).stream().mapToInt(Integer::parseInt).toArray();
		String tijd = arg.get(0).get("t");
		int value =Integer.parseInt( arg.get(0).get("v"));
		int id =idcreator;
		String name = arg.get(0).get("n");
		boolean dn =  Boolean.getBoolean(arg.get(0).get("dn"));
		idcreator++;
		// centraleId, String dag[], String tijd, String gordijn, int value)
		planningen.add(new Planning(centraleId, dag, tijd, gordijn, value, id, name, dn));
		// unlock
		editLock.unlock();
	}

	public static void delete(int msg) {
		// lock
		editLock.lock();
		for (int i = 0; i < planningen.size(); i++) {
			if (planningen.get(i).getPlanId() == msg) {
				planningen.remove(i);
			}
		}
		// unlock
		editLock.unlock();
	}

	// #getAllPlanning@c:1|u:ali$
	public static String getAllPlanning(int centrale) {
		editLock.lock();
		ArrayList<Planning> parts = new ArrayList<Planning>();
		for (int i = 0; i < planningen.size(); i++) {
			if (planningen.get(i).getCentraleId() == centrale) {
				parts.add(planningen.get(i));
			}
		}
		editLock.unlock();
		// message factory

		// #Planning@p:1|c:1|d:1,3,4,6|t:14-30|v:10&p:2|c:1|d:3,4,5,6|t:08-30|v:100$
		String msg = MessageFactory.createReturnPlanningMessage(parts);

		return msg;
	}

	@Override
	public void run() {
		System.out.println("planningthread Started");
		String lastTime = "00-00";
		
		String currentTime = Calendar.HOUR_OF_DAY + "-" + Calendar.MINUTE;
		int currentDay;
		// loop every 10 seconds
		while (true) {
			// lock
			System.out.println("planningthread check");
			currentDay = LocalDateTime.now().getDayOfWeek().getValue();
			editLock.lock();
			if (currentTime != lastTime) {
				
				currentTime = LocalDateTime.now().getHour() + "-" +  LocalDateTime.now().getMinute();
				System.out.println(currentTime+" "+currentDay);
				for (int i = 0; i < planningen.size(); i++) {
					for (int d = 0; d < planningen.get(i).getDag().length; d++) {
						if (planningen.get(i).getDag()[d] == currentDay) {
							System.out.println("dag"+d);
							System.out.println(planningen.get(i).getTijd() +" "+currentTime);
							if (planningen.get(i).getTijd().equals(currentTime)  && !currentTime.equals(lastTime)) {
								lastTime = currentTime;
								System.out.println("planning uitvoeren");
								int centralId = planningen.get(i).getCentraleId();
								int curtainId = planningen.get(i).getGordijn();
								int position = planningen.get(i).getGordijnStand();
								String pName = planningen.get(i).getNaam();
								boolean pDN = planningen.get(i).isDn();
								String bericht = MessageFactory.createSetCurtainMessage(centralId, curtainId, position, pName,pDN);
								if(ClientHandler.clients.get(ClientType.central).containsKey(centralId)){
								System.out.println("sending planning");
									ClientHandler.clients.get(ClientType.central).get(centralId).sendData(bericht);
								ClientHandler.clients.get(ClientType.central).get(centralId).getData();
								}
							}
						}
					}

				}
			}
			try {
				// wait until all thread have passed, or until time expired
				editCon.await(10, TimeUnit.SECONDS);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			editLock.unlock();
			// unlock
		}
	}
}
