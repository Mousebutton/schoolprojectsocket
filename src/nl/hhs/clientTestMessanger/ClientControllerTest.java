package nl.hhs.clientTestMessanger;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import nl.hhs.connection.Connector;
import nl.hhs.connection.MessageParser;

import java.io.IOException;

public class ClientControllerTest {
    private Connector connector;

    @FXML
    private TextArea area;
    @FXML
    private TextField txtInput;
    @FXML
    private void handleSendButton(ActionEvent event) throws IOException {
        try {
            if(this.connector == null)
            {
                this.connector = new Connector(ClientTest.SERVERLOCATION, ClientTest.SERVERPORT);
                while(true)
                {
                    if(this.connector.hasMsg())
                    {
                        MessageParser messageParser = new MessageParser(this.connector.getData());
                        if("getClientType".equals(messageParser.getCommand()))
                        {
                            System.out.println("Request ClientTest Type");
                            this.connector.sendData("#ClientType@ct:client|id:1$");
                        }
                        break;
                    }
                }
                this.addLineToConsole("Connected to localhost");
            }
            connector.sendData(this.txtInput.getText());
            this.addLineToConsole("sending msg");
        } catch (IOException e) {
            e.printStackTrace();
            this.addLineToConsole("Cannot connect to server.");
        }
        this.connector.close();
        this.connector = null;
    }
    private void addLineToConsole(String text)
    {
        this.area.appendText(text+System.lineSeparator());
    }
}
