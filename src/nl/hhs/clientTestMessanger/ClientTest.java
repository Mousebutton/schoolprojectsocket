package nl.hhs.clientTestMessanger;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import nl.hhs.connection.Connector;
import nl.hhs.server.Server;

import java.io.IOException;
import java.io.PrintWriter;

public class ClientTest extends Application {
    private Connector connection;
    private PrintWriter output;
    public static final String SERVERLOCATION = "145.52.160.118";
    public static final int SERVERPORT = Server.serverPort;

    @Override
    public void start(Stage primaryStage) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("ClientTest.fxml"));
        primaryStage.setResizable(false);
        primaryStage.setTitle("ClientTest");
        primaryStage.setScene(new Scene(root,900,500));
        primaryStage.show();
    }
    public static void main(String[] args) {
        launch(args);
    }

    public ClientTest() throws IOException {
        /*this.connection = new Connector("localhost",8010);

        BufferedReader consoleIn = new BufferedReader(new InputStreamReader(System.in));
        String fromServer;
        String fromClient = "";

        while(true)
        {
            try {
                fromClient = consoleIn.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if(fromClient != null)
            {
                if(fromClient.equals("exit"))
                    break;
                System.out.println("Sending: "+fromClient);
                this.connection.sendData(fromClient);
                if(fromClient.equalsIgnoreCase(ConnectionStatus.CLOSE.toString()))
                {
                    System.out.println("disconnecting");
                    break;
                }
            }
        }

        System.out.println("ClientTest disconnecting");*/
    }
}
