package nl.hhs.connection;

import java.io.*;
import java.net.Socket;

import nl.hhs.server.log;

public class Connector {

	public Socket connection;
	private BufferedReader input;
	private PrintWriter output;

	public Connector(Socket connection) throws IOException {
		this.connection = connection;
		this.input = new BufferedReader(new InputStreamReader(this.connection.getInputStream()));
		this.output = new PrintWriter(this.connection.getOutputStream(), true);
	}

	public Connector(String targetAddress, int portNumber) throws IOException {
		this(new Socket(targetAddress, portNumber));
	}

	public void sendData(String data) {
		if (!data.equals("getHey") && !data.equals("hey") && !data.equals("open")) {
			log.write("Sending: "+data);
		}
		this.output.println(data);
	}

	public boolean isAlive() {
		this.sendData("getHey");
		long startTime = System.currentTimeMillis();

		while (true) {
			if (this.hasMsg()) {
				String msg = this.getData();
				return ConnectionStatus.fromString(msg) == ConnectionStatus.ISOPEN;
			} else if (System.currentTimeMillis() - startTime >= 3000) {
				return false;
			}
		}
	}
	


	public String getData() {
		StringBuilder sb = new StringBuilder();
		String msg;
		while (this.hasMsg()) {
			try {
				sb.append(this.input.readLine());
			} catch (IOException e) {
				e.printStackTrace();
				return "";
			}
		}
		msg = sb.toString();
		System.out.println("getting data:" + msg);
		if (!sb.toString().equals("getHey") && !sb.toString().equals("hey") && !sb.toString().equals("open") && !sb.toString().equals("")) {
			//System.out.println(sb.toString());
			log.write("Recieving: "+msg);
		}
		return msg;
	}

	public boolean hasMsg() {
		try {
			return this.connection.getInputStream().available() > 0;
		} catch (IOException e) {
			return false;
		}
	}

	public void close() throws IOException {
		this.connection.close();
	}
}
