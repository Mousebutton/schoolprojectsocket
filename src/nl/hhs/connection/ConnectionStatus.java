package nl.hhs.connection;

public enum ConnectionStatus {

    CLOSE("disconnect"),
    OPEN("open"),
    ISOPEN("hey");

    private String status;
    ConnectionStatus(String s)
    {
        this.status = s;
    }

    @Override
    public String toString()
    {
        return this.status;
    }

    public static ConnectionStatus fromString(String s)
    {
        for (ConnectionStatus b : ConnectionStatus.values()) {
            if (b.toString().equalsIgnoreCase(s)) {
                return b;
            }
        }
        return null;
    }
}
