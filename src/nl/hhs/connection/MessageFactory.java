package nl.hhs.connection;

import nl.hhs.central.Curtain;

import java.util.Arrays;

import java.util.List;

import nl.hhs.server.Planning;


public class MessageFactory {

    //#Curtain@c:1|g:1|n:Gordijn in de keuken|v:80|dn:true$
    public static String createCurtainsMessage(int centralId, Curtain... curtains)
    {
        StringBuilder msg = new StringBuilder("#Curtain@");
        for (int i = 0; i < curtains.length; i++) {
            Curtain curtain = curtains[i];
            msg.append(String.format("c:%s|g:%s|n:%s|v:%s|dn:%s", centralId, curtain.getId(),
                    curtain.getName(), curtain.getPosition(), curtain.isDayAndNight()));
            msg.append(i == curtains.length - 1 ? "$" : "&");
        }
        return msg.toString();
    }

    public static String createSetCurtainMessage(int centralId, int curtainId,int position, String name, boolean dn)
    {
        StringBuilder msg = new StringBuilder("");
        msg.append("#setCurtain@");
        msg.append("c:"+centralId);
        msg.append("|u:test");
        msg.append("|g:"+curtainId);
        msg.append("|n:"+ name);
        msg.append("|dn:"+ dn);
        msg.append("|v:"+position+"$");

        return msg.toString();
    }

    //#SensorData@c:1|u:test|g:1|d:250$
    public static String createSensorDataMessage(int centralId, int curtainId, String rawSensorData) {
        StringBuilder msg = new StringBuilder("");
        msg.append("#SensorData@");
        msg.append("c:" + centralId);
        msg.append("|u:test");
        msg.append("|g:" + curtainId);
        msg.append("|d:" + rawSensorData.split(" ")[1]);
        msg.append("$");
        return msg.toString();
    }
    
    public static String getSensorDataMessage(int centralId){
    	return "#getSensorData@"+centralId;
    }



    //#Planning@p:1|c:1|d:1,3,4,6|t:14-30|g:1|v:10&p:2|c:1|d:3,4,5,6|t:08-30|g:1|v:100$
    public static String createReturnPlanningMessage(List<Planning> plan){
		StringBuilder msg = new StringBuilder("#Planning@");
		for(int i = 0; i < plan.size();i++){
			msg.append("p:"+ plan.get(i).getPlanId());
			msg.append("|c:"+ plan.get(i).getCentraleId());
            msg.append("|g:"+ plan.get(i).getGordijn());
            msg.append("|n:"+ plan.get(i).getNaam());
			msg.append("|d:"+ Arrays.toString(plan.get(i).getDag()) .replace("[","")
                                                                    .replace("]","")
                                                                    .replace(" ",""));
            msg.append("|t:"+ plan.get(i).getTijd());
			msg.append("|v:"+ plan.get(i).getGordijnStand());
            msg.append("|dn:"+ plan.get(i).isDn());
			msg.append("&");
		}

		if(plan.size() != 0){
            msg.deleteCharAt(msg.length()-1);
        }
        msg.append("$");

		
    	return msg.toString();
    }
    
    // #setPlanning@c:1|u:ali|g:1|d:1,2,6,7|t:14-10|v:80$
    public static String createSetPlanningMessage(Planning planning){
    	StringBuilder msg = new StringBuilder("#setPlanning@");
    	msg.append("c:"+ planning.getCentraleId());
    	//msg.append("u:"+ planning.);
    	msg.append("|g:"+ planning.getGordijn());
    	msg.append("|d:"+ Arrays.toString(planning.getDag()).trim());
    	msg.append("|t:"+ planning.getTijd());
    	msg.append("|v:"+ planning.getGordijnStand());
    	msg.append("$");
    	return msg.toString();
    }
    // #getAllPlanning@c:1|u:ali$
    public static String createAllPlanning(String centrale){
    	return "#getAllPlanning@c:"+centrale+"$";
    }
    
    // #deletePlanning@c:1|u:ali|p:1$ 
    public static String createDeletePlanningMessage(String centrale, String id){
    	
    	return "#deletePlanning@c:"+ centrale+"|p:"+id+"$";
    }
}
