package nl.hhs.connection;

import java.util.*;

public class MessageParser {
	private String rawData;
	
	public MessageParser(String line) {
		this.rawData = line;
	}
	private boolean isValid()
	{
		return true;
	}
	public String getCommand()
	{
		if(isValid())
		{
			return this.rawData.split("@")[0].substring(1);
		}
		return "";
	}

	public ArrayList<HashMap<String,String>> getArgs()
	{
		ArrayList<HashMap<String,String>> allData = new ArrayList<>();
		if(isValid())
		{
			// e.g. "#getAllCurtains@c:1|u:ali|g:1|d:1,2,6,7|t:14-10|v:80&c:1|u:ali|g:3|d:3,4,5|t:12-30|v:60$"

			//cut the start end end marker off
			String dataWithoutMarkings = this.rawData.substring(1,this.rawData.length()-1);
			//go further with argument line, which will be split to single unit in the case that
			//are more unites
			if(dataWithoutMarkings.split("@").length == 2)
			{
				String[] argLines = dataWithoutMarkings.split("@")[1].split("&");

				for (String argLine : argLines) {
					HashMap<String,String> argPairMap = new HashMap<>();
					for (String arg : argLine.split("\\|")) {
						String[] argPair = arg.split(":");
						argPairMap.put(argPair[0], argPair[1]);

					}
					allData.add(argPairMap);
				}
			}
		}
		return allData;
	}
}
/*
 * #c = Centrale ,u = user , set or get, v = value , g = curtain;
 *

 * Connection details
 * #getClientType@$
 * #ClientType@ct:client|id:1$
 * #ClientType@ct:central|id:1$
 *

 * ClientTest to Server
 * Curtain ClientTest to Server || Server to central #command@central:id|user:name$
 * #setCurtain@c:1|u:ali|g:1|n:Gordijn in de keueken|v:100|dn:true$
 * #command@|central:id|user:name|curtain:id|n:nameOfCurtain|value:0-100%|dn:true/false$
 * #getAllCurtains@c:1|u:ali$ ->Curtain[]
 *
 * Server to ClientTest || Central to serves
 *  (
 * return)

 * #Curtain@c:1|g:1|v:100&c:1|g:1|v:100$
 *
 * Planning ClientTest to Server
 * #setPlanning@c:1|u:ali|g:1|d:1,2,6,7|t:14-10|v:80$
 * #deletePlanning@c:1|u:ali|p:1$ #getAllPlanning@c:1|u:ali$
 * 
 * Server to ClientTest
 * #Planning@p:1|c:1|d:1,3,4,6|t:14-30|v:10&p:2|c:1|d:3,4,5,6|t:08-30|v:100$ (
 * return)
 * 
 * #command@central:id|user:name|curtain:id|days:1(monday),2,3,4,5,6,7(sunday)|
 * time:timeat(hh-mm)|value:0-100%$
 */
