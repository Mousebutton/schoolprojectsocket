package intergration;

import nl.hhs.connection.MessageParser;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.*;

import static org.junit.Assert.assertEquals;

public class IntegrationTestRegisterClientToServer {

    private ServerController server;
    private Client client;

    @Before
    public void setData() throws IOException {
        server = new ServerController();
        client = new Client();
    }

    /**
     * This test checks whether the client is correctly registered.
     *
     */
    @Test
    public void checkIfClientIsRegisterdByServer() throws IOException, InterruptedException {
        server.startSocketServer();
        client.handleSendButton();
        assertEquals("Result: ",1, server.connectedId);
    }

    public class ServerController {
        private ServerSocket serverSocket;
        private Socket clientSocket;

        private Thread serverThread = null;
        private int serverport = 8010;
        public int connectedId = 0;

        private void startSocketServer()
        {

            if(serverThread == null)
            {
                serverThread = new Thread(() -> {

                    try {
                        serverSocket = new ServerSocket(this.serverport);
                        serverSocket.setSoTimeout(5000);
                    } catch (IOException e) {
                        e.printStackTrace();
                        System.exit(-1);
                    }
                    while(!Thread.currentThread().isInterrupted())
                    {
                        try {
                            clientSocket = serverSocket.accept();
                            System.out.println("new connection");
                            ClientHandler client = new ClientHandler(clientSocket);
                            connectedId = client.getConnectedId();
                        } catch (SocketTimeoutException e) {
                            System.out.println("No new connection made");
                        }
                        catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    System.out.println("Server stopped.");
                });
                serverThread.start();
            }
        }

    }

    public class ClientHandler{

        nl.hhs.server.ClientHandler.ClientType connectedType;

        public int getConnectedId() {
            return connectedId;
        }

        private int connectedId = 0;

        //storage with connectors per clientType such as client and central
        public Map<nl.hhs.server.ClientHandler.ClientType, Map<Integer, Connector>> clients = new HashMap<>();
        {
            clients.put(nl.hhs.server.ClientHandler.ClientType.central, new HashMap<>());
            clients.put(nl.hhs.server.ClientHandler.ClientType.client, new HashMap<>());
        }

        private Connector connection;

        public ClientHandler(Socket serverSocket) throws IOException {
            connection = new Connector(serverSocket);
            System.out.println(connection.connection);
            connection.sendData("#getClientType@$");

            MessageParser messageParser;
            while(true)
            {
                if(this.connection.hasMsg())
                {
                    messageParser = new MessageParser(this.connection.getData());
                    break;
                }
            }

            if("ClientType".equals(messageParser.getCommand()))
            {
                System.out.println("Request ClientTest Type");
                if("central".equals(messageParser.getArgs().get(0).get("ct")))
                {
                    connectedType = nl.hhs.server.ClientHandler.ClientType.central;
                    clients.get(connectedType).put(Integer.parseInt(messageParser.getArgs().get(0).get("id")),this.connection);
                } else {
                    connectedType = nl.hhs.server.ClientHandler.ClientType.client;
                    clients.get(connectedType).put(Integer.parseInt(messageParser.getArgs().get(0).get("id")),this.connection);
                    connectedId = Integer.parseInt(messageParser.getArgs().get(0).get("id"));
                }
            }
        }

    }

    public class Client {
        private Connector connector;

        //method which is normally from GUI with a button invoked
        private void handleSendButton() throws IOException, InterruptedException {
            try {
                if(this.connector == null)
                {
                    this.connector = new Connector("localhost",8010);

                    while(true)
                    {
                        if(this.connector.hasMsg())
                        {
                            MessageParser messageParser = new MessageParser(this.connector.getData());
                            if("getClientType".equalsIgnoreCase(messageParser.getCommand()))
                            {
                                System.out.println("Request ClientTest Type");
                                this.connector.sendData("#ClientType@ct:client|id:1$");
                            }
                            break;
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();

            }
            this.connector.close();
            this.connector = null;
        }

    }

    //This class will take care of sending and receiving messages for both client and server.
    public class Connector {

        public Socket connection;
        private BufferedReader input;
        private PrintWriter output;

        public Connector(Socket connection) throws IOException {
            this.connection = connection;
            this.input = new BufferedReader(new InputStreamReader(this.connection.getInputStream()));
            this.output = new PrintWriter(this.connection.getOutputStream(), true);
        }

        public Connector(String targetAddress, int portNumber) throws IOException {
            this(new Socket(targetAddress, portNumber));
        }

        public void sendData(String data) {

            this.output.println(data);
        }

        public String getData() {
            StringBuilder sb = new StringBuilder();
            String msg;
            while (this.hasMsg()) {
                try {
                    sb.append(this.input.readLine());
                } catch (IOException e) {
                    e.printStackTrace();
                    return "";
                }
            }
            msg = sb.toString();
            System.out.println("getting data:" + msg);
            return msg;
        }

        public boolean hasMsg() {
            try {
                return this.connection.getInputStream().available() > 0;
            } catch (IOException e) {
                return false;
            }
        }

        public void close() throws IOException {
            this.connection.close();
        }


    }

}