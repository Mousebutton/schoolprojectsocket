package intergration;

import nl.hhs.connection.ConnectionStatus;
import nl.hhs.connection.MessageParser;

import nl.hhs.server.log;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;

import static org.junit.Assert.assertEquals;

public class IntegrationTestPingingCentralToServer {

    private ServerController server;
    private Central central;

    @Before
    public void setData() throws IOException {
        server = new ServerController();
        central = new Central();
    }

    /**
     * This test will check if the handshake between the central and server works correctly.
     */
    @Test
    public void checkServerResponseIfItsHey() throws IOException {
        server.startSocketServer();
        Connector serverConnection = central.start();
        assertEquals("Result: ","hey", central.getPing(serverConnection));
    }

    public class ServerController {
        private ServerSocket serverSocket;
        private Socket clientSocket;

        private Thread serverThread = null;
        private int serverport = 8010;

        private void startSocketServer()
        {

            if(serverThread == null)
            {
                serverThread = new Thread(() -> {

                    try {
                        serverSocket = new ServerSocket(this.serverport);
                        serverSocket.setSoTimeout(5000);
                    } catch (IOException e) {
                        e.printStackTrace();
                        System.exit(-1);
                    }
                    while(!Thread.currentThread().isInterrupted())
                    {
                        try {
                            clientSocket = serverSocket.accept();
                            System.out.println("new connection");
                            ClientHandler client = new ClientHandler(clientSocket);
                            client.start();
                        } catch (SocketTimeoutException e) {
                            System.out.println("No new connection made");
                        }
                        catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    System.out.println("Server stopped.");
                });
                serverThread.start();
            }
        }

    }

    //ClientHandler manage the clients for the server such as client and central
    public class ClientHandler extends Thread {

        private Connector connection;

        public ClientHandler(Socket serverSocket) throws IOException {
            connection = new Connector(serverSocket);
            System.out.println(connection.connection);
        }

        public void run() {
            String msg;
            while (!Thread.currentThread().isInterrupted()) {
                if (this.connection.hasMsg()) {
                    msg = this.connection.getData();
                    System.out.println(msg);
                    MessageParser s = new MessageParser(msg);

                    // Pinging hey
                    if (msg.equals("getHey")) {
                        connection.sendData(ConnectionStatus.ISOPEN.toString());
                    }
                }

                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public class Central {

        public Connector start() {
            while(true){
                try{
                    //just to be sure that connection is established after the handshake
                    return new Connector("localhost",8010);
                }catch(IOException e){
                    System.out.println(Thread.currentThread().getId()+"-"+"Server is not available. Busy with reconnecting ...");
                    try {
                        Thread.sleep(3000);
                    } catch (InterruptedException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        }

        // The method isAlive() will return true if the hey message is received. The implementation of this method is in
        //Connector class of connection package
        public String getPing(Connector serverConnection){
            if(serverConnection.isAlive()){
                System.out.println(Thread.currentThread().getId()+"-"+"Server connection is established\n");
                return "hey";
            }
            return "";
        }

    }

    //This class will take care of sending and receiving messages for both client and server.
    public class Connector {

        public Socket connection;
        private BufferedReader input;
        private PrintWriter output;

        public Connector(Socket connection) throws IOException {
            this.connection = connection;
            this.input = new BufferedReader(new InputStreamReader(this.connection.getInputStream()));
            this.output = new PrintWriter(this.connection.getOutputStream(), true);
        }

        public Connector(String targetAddress, int portNumber) throws IOException {
            this(new Socket(targetAddress, portNumber));
        }

        public void sendData(String data) {
            if (!data.equals("getHey") && !data.equals("hey") && !data.equals("open")) {
                log.write("Sending: "+data);
            }
            this.output.println(data);
        }

        public boolean isAlive() {
            this.sendData("getHey");
            long startTime = System.currentTimeMillis();

            while (true) {
                if (this.hasMsg()) {
                    String msg = this.getData();
                    return ConnectionStatus.fromString(msg) == ConnectionStatus.ISOPEN;
                } else if (System.currentTimeMillis() - startTime >= 3000) {
                    return false;
                }
            }
        }


        public String getData() {
            StringBuilder sb = new StringBuilder();
            String msg;
            while (this.hasMsg()) {
                try {
                    sb.append(this.input.readLine());
                } catch (IOException e) {
                    e.printStackTrace();
                    return "";
                }
            }
            msg = sb.toString();
            System.out.println("getting data:" + msg);
            if (!sb.toString().equals("getHey") && !sb.toString().equals("hey") && !sb.toString().equals("open") && !sb.toString().equals("")) {
                log.write("Recieving: "+msg);
            }
            return msg;
        }

        public boolean hasMsg() {
            try {
                return this.connection.getInputStream().available() > 0;
            } catch (IOException e) {
                return false;
            }
        }


    }


}