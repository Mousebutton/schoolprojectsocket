package unit;

import nl.hhs.connection.MessageFactory;
import nl.hhs.server.Planning;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static org.junit.Assert.assertEquals;
@RunWith(Parameterized.class)
public class TestPlanningThreadsGetAllPlaningMethod {

    // fields used together with @Parameter must be public
    @Parameterized.Parameter()
    public int testedInput;
    @Parameterized.Parameter(1)
    public String result;


    // creates the test data
    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        Object[][] data = new Object[][] {
                { 1, "#Planning@p:1|c:1|g:1|n:Curtain in the kitchen|d:1,2,3,4|t:14-10|v:60|dn:true&p:2|c:1|g:2|n:Curtain in the bedroom|d:1,2,3,4|t:17-45|v:100|dn:false$"},
                { 4,  "#Planning@$"}};
        return Arrays.asList(data);
    }

    //This test two inputs an their responses the inputs are based on condition coverage
    @Test
    public void testVariousInputCheckWithPredictedResults () {
     PlaningThread tester = new PlaningThread();
        // #setPlanning@c:1|u:ali|g:1|d:1,2,6,7|t:14-10|v:80$
        int[] days= {1,2,3,4};
        tester.planningen.add(new Planning(1, days,
                "14-10" , 1, 60, 1, "Curtain in the kitchen", true));
        tester.planningen.add(new Planning(1, days,
                "17-45", 2, 100, 2,"Curtain in the bedroom", false));
        assertEquals("Result", result, tester.getAllPlanning(testedInput));
    }


    // class to be tested
    class PlaningThread {
        public ArrayList<Planning> planningen = new ArrayList<Planning>();
        private final Lock editLock = new ReentrantLock();

        /**
         * This method will return a message protocol string with all plannings in it per given central id as string
         * @param centrale id string like 1, 2 or 3
         * @return e.g. Planning@p:1|c:1|d:1,3,4,6|t:14-30|v:10&p:2|c:1|d:3,4,5,6|t:08-30|v:100$
         */
        public String getAllPlanning(int centrale) {

            editLock.lock();
            ArrayList<Planning> parts = new ArrayList<>();

            for (int i = 0; i < planningen.size(); i++) {

                if (planningen.get(i).getCentraleId() == centrale) {
                    parts.add(planningen.get(i));
                }

            }
            editLock.unlock();
            System.out.println(MessageFactory.createReturnPlanningMessage(parts));
            // #Planning@p:1|c:1|d:1,3,4,6|t:14-30|v:10&p:2|c:1|d:3,4,5,6|t:08-30|v:100$
            return MessageFactory.createReturnPlanningMessage(parts);
        }
    }
}
