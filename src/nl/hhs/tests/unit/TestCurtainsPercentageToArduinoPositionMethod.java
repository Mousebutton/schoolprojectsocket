package unit;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

/**
 * This test class will test it's percentageToArduinoPosition method with condition code coverage
 */
@RunWith(Parameterized.class)
public class TestCurtainsPercentageToArduinoPositionMethod {

    // fields used together with @Parameter must be public
    @Parameterized.Parameter()
    public int testedInput;
    @Parameterized.Parameter(1)
    public int result;


    // creates the test data
    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        Object[][] data = new Object[][] { { 0, 0}, { 7, 13}, {101, -1} };
        return Arrays.asList(data);
    }

    //Condition coverage
    @Test
    public void testVariousInputCheckWithPredictedResults() {
        Curtain tester = new Curtain();
        assertEquals("Result", result, tester.percentageToArduinoPosition(testedInput));
    }


    // class to be tested
    class Curtain {
        /**
         * This method is used to convert the percentage value to the acceptable value for arduino servo because
         * the max range from servo is 180 degrees. So we make 180/100*percentage = the result is representative percantge
         * for Arduino.
         * @param percentage integer between 0 - 100
         * @return return 0 if the arg was 0 too or a value converted to arduino servo format. And if the arg was larger than
         * 100 or below 0 return -1 for an invalid command
         */
        public int percentageToArduinoPosition(int percentage) {
            final double MAX_ROTATION = 180.00;
            final double CONVERSION_RATE = MAX_ROTATION/100.00;

            if(percentage >= 0 && percentage < 101){
                if(percentage != 0){
                    return (int) Math.round(CONVERSION_RATE * (double) percentage);
                }else{return 0;}
            }else{
                return -1;
            }
        }
    }

}

