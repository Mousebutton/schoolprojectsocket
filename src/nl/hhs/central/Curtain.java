package nl.hhs.central;

import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;

/**
 * This class represent curtain where name could be "curtain in the kitchen" or position is in the percentage so 0 is for
 * 0% and 100 is for %100. DayAndNight boolean value is to denote if the curtain has to be used for evaluation in the
 * DayAndNight thread. The methods are mainly getters and setters and the other methods are for converting the percentage
 * values to values which can the arduino servo accept or methods to examine the sensor values about the curtain for
 * DayAndNight thread.
 */
public class Curtain {

    private String name;
    private int id;
    private int position;
    private boolean dayAndNight;

    public Curtain(int id, String name, int position, boolean dayAndNight) {
        this.id = id;
        this.name = name;
        this.position = position;
        this.dayAndNight = dayAndNight;
    }

    public boolean isDayAndNight() {
        return dayAndNight;
    }

    public int getPosition() {
        return position;
    }
    
    public String getName() {
        return name;
    }
    
    public int getId() {
        return id;
    }

    public void setPosition(int position) {
            this.position = position;
    }
    
    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDayAndNight(boolean dayAndNight) {
        this.dayAndNight = dayAndNight;
    }

    /**
     * This method is used to convert the percentage value to the acceptable value for arduino servo because
     * the max range from servo is 180 degrees. So we make 180/100*percentage = the result is representative percantge
     * for Arduino.
     * @param percentage integer between 0 - 100
     * @return return 0 if the arg was 0 too or a value converted to arduino servo format. And if the arg was larger than
     * 100 or below 0 return -1 for an invalid command
     */
    public static int percentageToArduinoPosition(int percentage) {
        final double MAX_ROTATION = 180.00;
        final double CONVERSION_RATE = MAX_ROTATION/100.00;

        if(percentage >= 0 && percentage < 101){
            if(percentage != 0){
                return (int) Math.round(CONVERSION_RATE * (double) percentage);
            }else{return 0;}
        }else{
            return -1;
        }
    }

    /**
     * This method will iterate trough HashSet of curtains and if the property dayAndNight of curtain is true than
     * will be on the curtain evoked method to evaluate sensor data tryExecuteDayAndNight method.
     * @param curtains HashSet curtains to examine for DayAndNight thread
     * @param ac Arduino connector to pass it to the tryExecuteDayAndNight method
     */
    public static synchronized void examineDayAndNight(HashSet<Curtain> curtains, ArduinoConnector ac){
        Iterator<Curtain> it = null;
        if (curtains != null) {
            it = curtains.iterator();
        }
        if (it != null) {
            while (it.hasNext()) {
                Curtain temp = it.next();
                if (temp.isDayAndNight()) {
                	System.out.println(temp.tryExecuteDayAndNight(ac));
                    if (temp.tryExecuteDayAndNight(ac)) {
                        System.out.println(Thread.currentThread().getId()+"-"+temp.name + " dayAndNight executed  END");
                    } else {
                        System.out.println(Thread.currentThread().getId()+"-"+temp.name + " dayAndNight cannot be executed at this moment END");
                    }
                }
            }
        }
    }

    /**
     * This method is used to evaluate sensor data and based on that take an action such as close, open or set
     * curtain and the changed the position value in the register.
     * @param ac Arduino connector to ask for sensor data to evaluate them.
     * @return true or false for if the try is executed or not
     */
    private boolean tryExecuteDayAndNight(ArduinoConnector ac){
        System.out.println("\n" + Thread.currentThread().getId()+"-"+"DayAndNight check  START");
        ac.write("d");
        int formattedResponse = Integer.parseInt(ac.getResponse().split(" ")[1]);

        if (formattedResponse < 150 && this.position != 0){
            ac.write("c");
            int response = Integer.parseInt(ac.getResponse());
            if(response == 0){
                try {
                    this.position = 0;
                    Curtain temp = this;
                    JSONRegisterManager.deleteCurtain(this.id);
                    JSONRegisterManager.encodeCurtain(temp);
                } catch (IOException e) {
                    e.printStackTrace();
                    return false;
                }
                return true;
            }else{return false;}

        }else if (formattedResponse > 150 && this.position != 100) {
            ac.write("o");
            int response = Integer.parseInt(ac.getResponse());
            if(response == 180){
                try {
                    this.position = 100;
                    Curtain temp = this;
                    JSONRegisterManager.deleteCurtain(this.id);
                    JSONRegisterManager.encodeCurtain(temp);
                } catch (IOException e) {
                    e.printStackTrace();
                    return false;
                }
                return true;
            }else{return false;}
        }else{return false;}
    }

    public String toString(){
        return "Curtain: id:" + getId() + ", name:" + getName() + ", position: " + getPosition() + ", dayAndNight: " + isDayAndNight();
    }
}
