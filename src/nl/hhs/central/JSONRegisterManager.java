package nl.hhs.central;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * This class manage CurtainRegister that means filling register with JSON object lines with curtain data or
 * other way around from register mapping the JSON object data to the HashSet of curtains objects. We can delete and edit
 * a single curtain from register.
 */
public class JSONRegisterManager {
	 private static String path= 
	    //path = "";
	    //path = "/Users/patriksnajdr/schoolprojectsocket/src/nl/hhs/central/CurtainRegister.txt";
	    "C:\\git\\schoolProject\\src\\nl\\hhs\\central\\CurtainRegister.txt";
	
	@SuppressWarnings("unchecked")

    /**
     * This method wil encode the Curtain object into JSON object and write into the CurtainRegister file
     * At the end of this method will be user informed about the written data into register.
     */
   
    
    public static void encodeCurtain(Curtain curtain) throws IOException {
        JSONObject obj = new JSONObject();
        obj.put("Id", curtain.getId());
        
      
        
        JSONArray properties = new JSONArray();
        properties.add("Name:" + curtain.getName());
        properties.add("Position:" + curtain.getPosition());
        properties.add("DayAndNight:" + curtain.isDayAndNight());
        obj.put("Properties", properties);

        try (FileWriter file =
                     new FileWriter(path, true)) {
            file.write(obj.toJSONString() + System.getProperty("line.separator"));
            System.out.println("\nSuccessfully Copied JSON Object to File...");
            System.out.println("\nJSON Object: " + obj);
        }
    }

    /**
     * This method is used to get all data from register and mapped into HashSet with curtain objects
     * @return HashSet of cutrains from CurtainRegister file
     */
    public static HashSet<Curtain> decodeRegisterToHashSet() {
        // e.g. {"Id":1,"Properties":["Name :Gordijn in de keuken","Position :100","DayAndNight:false"]}
        JSONParser parser = new JSONParser();

        try {
            BufferedReader bufferreader = new BufferedReader(new FileReader(path));
            		
            		
            		
            String line;
            HashSet<Curtain> curtainSet = new HashSet();

            while ((line = bufferreader.readLine()) != null) {

                try {
                    Object obj = parser.parse(line);
                    JSONObject curtainObj = (JSONObject) obj;
                    JSONArray properties = (JSONArray) curtainObj.get("Properties");
                    int id = Integer.parseInt(String.valueOf(curtainObj.get("Id")));
                    String name = "";
                    int position = 0;
                    boolean dayAndNight = false;

                    //assigning the property values
                    for (int i = 0; i < properties.size(); i++) {
                        if (i == 0) {
                            String[] separatorArr = properties.get(i).toString().split(":");
                            name = separatorArr[1];
                        } else if (i == 1) {
                            String[] separatorArr = properties.get(i).toString().split(":");
                            position = Integer.parseInt(separatorArr[1]);
                        } else {
                            String[] separatorArr = properties.get(i).toString().split(":");
                            dayAndNight = Boolean.parseBoolean(separatorArr[1]);
                        }
                    }

                    curtainSet.add(new Curtain(id, name, position, dayAndNight));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return curtainSet;
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    /**
     * This method based on the id will delete the curtain from the CurtainRegister file
     * @param curtainId identifier to find curtain which has to be deleted
     */
    public static synchronized void deleteCurtain(int curtainId) {
        Set<Curtain> curtainSet = JSONRegisterManager.decodeRegisterToHashSet();

        if (curtainSet != null) {
            curtainSet.forEach((Curtain s) -> {
                if (s.getId() == curtainId) {
                    curtainSet.remove(s);
                }
            });

            PrintWriter writer = null;
            try {
                writer = new PrintWriter(path);
            } catch (FileNotFoundException e) {
                try {
                    File file = new File (path);
                    if(file.createNewFile()) {
                        System.out.println("CurtainRegister.txt is successful created!");
                    }
                    else {
                        System.out.println("CurtainRegister.txt is failed to create!");
                    }
                }
                catch (IOException es) {
                    es.printStackTrace();
                }

            }
            //empty register file
            writer.print("");
            writer.close();

            //encode rest of the curtains back into register
            curtainSet.forEach((Curtain s) -> {
                try {
                    encodeCurtain(s);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }
    }

    /**
     * This method based on arguments of curtain form the MessageParser will change the values of the curtain
     * in the register
     * @param argLineList arugment list of single curtain values that has to be changed.
     * @return true if the curtain is changed or false if it's not found
     */
    public static synchronized boolean editCurtain(ArrayList<HashMap<String, String>> argLineList) {
        try {
            HashSet<Curtain> curtains = decodeRegisterToHashSet();

            int curtainId = Integer.parseInt(argLineList.get(0).get("g"));
            Iterator<Curtain> it = null;
            if (curtains != null) {
                it = curtains.iterator();
            }
            if (it != null) {
                while (it.hasNext()) {
                    //if there is a matching curtain delete the curtain object en make a new one with
                    //with changed properties

                    Curtain temp = it.next();
                    if (temp.getId() == curtainId) {
                        temp.setName(argLineList.get(0).get("n").trim());
                        temp.setPosition(Integer.parseInt(argLineList.get(0).get("v")));
                        temp.setDayAndNight(Boolean.parseBoolean(argLineList.get(0).get("dn")));
                        deleteCurtain(curtainId);
                        encodeCurtain(temp);
                        return true;
                    } else {
                        return false;
                    }
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

}
