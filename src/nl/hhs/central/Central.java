package nl.hhs.central;

import nl.hhs.connection.Connector;
import nl.hhs.connection.MessageParser;
import java.net.ConnectException;

/**
 * This main method starts thread for checking the light sensor value if there
 * is a certain value which is below or above a day and night limit the action
 * will be made by Arduino.
 *
 * Second thread make a server connection and initial handshake with the server
 * and reconnect if it is needed. Commands from server will be handled and
 * evaluated to the server.
 */
public class Central {
    public static void main(String[] args) throws Exception {

		ArduinoConnector ac = new ArduinoConnector();
		Thread thread = new DayAndNightSchedulerThread(ac);
		thread.start();

		while (true) {
			try {
				Connector serverConnection = new Connector("145.52.160.118", 8010);

				// if the handshake is not accomplished within 3 sec than try
				// connect to the server again.
				long startTime = System.currentTimeMillis();
				while (true) {
					if (serverConnection.hasMsg()) {
						MessageParser mp = new MessageParser(serverConnection.getData());
						if (mp.getCommand().equals("getClientType")) {
							serverConnection.sendData("#ClientType@ct:central|id:1$");
							System.out.println("\n" + Thread.currentThread().getId() + "-" + "ClienType sent");
							break;
						}
					} else if (System.currentTimeMillis() - startTime >= 5000) {
						System.out.println(1 + "exception");
						throw new ConnectException();
					}
				}

				// just to be sure that connection is established after the
				// handshake
				if (serverConnection.isAlive()) {
					System.out.println(Thread.currentThread().getId() + "-" + "Server connection is established\n");
				}

				// Check a connection every 20 sec if not try to connect again,
				// otherwise do the simple validation of
				// command message and pass it to it's handler
				long counter = System.currentTimeMillis();
				while (true) {
					if (!(System.currentTimeMillis() - counter >= 20000)) {
						validateAndPassCommand(ac, serverConnection);
					} else {
						counter = System.currentTimeMillis();
						if (serverConnection.isAlive()) {
							validateAndPassCommand(ac, serverConnection);
						} else {
							System.out.println(2 + "exception");
							throw new ConnectException();
						}
					}
				}
			} catch (ConnectException e) {
				System.out.println(
						Thread.currentThread().getId() + "-" + "Server is not available. Busy with reconnecting ...");
				Thread.sleep(5000);
			}
		}
	}


	/**
	 * This method is wrapper to reuse it many times in the code so that we save
	 * a space, but the working of this method is to do simple validation if the
	 * message is not empty and to pass it the message to the command handler
	 * and evaluate it to the server
	 * 
	 * @param ac
	 *            Arduino connector which has to be passed in order to use it
	 *            because we have to use only one ac for the application. If we
	 *            want more of ac connectors is it not possible because port is
	 *            already opened or we have to close it every single time and
	 *            open it again and than is here race condition with dayAndNight
	 *            thread which will ac use it either.
	 * @param serverConnection
	 *            we use this connector to send the evaluation of command back
	 *            to the server
	 */
	private static void validateAndPassCommand(ArduinoConnector ac, Connector serverConnection) {
		if(!serverConnection.hasMsg()){
			return;
		}
		String command = serverConnection.getData();
		if (!command.equals("")) {
			System.out.println("\n" + Thread.currentThread().getId() + "-" + "command om te uitvoeren" + command);

			if (!CommandHandler.handle(command, ac, serverConnection)) {
				serverConnection
						.sendData(Thread.currentThread().getId() + "-" + "This " + command + " cannot be executed");
			} else {
				serverConnection.sendData(Thread.currentThread().getId() + "-" + "This " + command + " is executed");
			}

		}
	}
}
