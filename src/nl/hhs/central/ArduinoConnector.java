package nl.hhs.central;

import gnu.io.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.*;

/**
 *This class make a serial connection with Arduino via the iterating through the available
 * ports checking for arudino port and try to open it and that everything in it's constructor.
 * Object of this class can invoke the write method to write a message to serial port. SerialEvent method
 * listens to the port and saves the messages for getResponse method.
 */
public class ArduinoConnector implements SerialPortEventListener  {
    private static final int TIME_OUT = 5000;
    private static final int DATA_RATE = 9600; // baud rate

    private SerialPort arduinoSerialPort;
    private BufferedReader input;
    private OutputStream output;
    private String inputLine;

    //most usual port names for arduino. Uncomment the options based on your OS.
    private static final String ARUDINO_PORT_NAMES[] = {
//            "/dev/tty.usbmodem", // Mac OS
//            "/dev/cu.usbmodem", // Mac OS X
//            "/dev/usbdev", // Linux
//            "/dev/tty", // Linux
//            "/dev/serial", // Linux
            "COM3", // Windows
    };

    public ArduinoConnector() {
        //these methods below will assign the properties and setup the ArduinoConnector
        //if it fails to open the port than every 5 sec will try it again till is it open.
        boolean isPortOpend = openArduinoPort(getAvailableSerialPorts());
        long counter = System.currentTimeMillis();
        while(true){
            if(!(isPortOpend) && System.currentTimeMillis() - counter >= 5000){
                counter = System.currentTimeMillis();
                isPortOpend = openArduinoPort(getAvailableSerialPorts());
            }else if(isPortOpend) {
                setUpArduinoPort();
                initializeIOstreams();
                break;
            }
        }
    }

    /**
     * This method will look if the are any serialPorts available and send the appropriate message
     * @return A HashSet containing the CommPortIdentifier for all serial ports that are not currently being used.
     */
    private static HashSet<CommPortIdentifier> getAvailableSerialPorts() {
        HashSet<CommPortIdentifier> h = new HashSet<>();
        Enumeration thePorts = CommPortIdentifier.getPortIdentifiers();
        while (thePorts.hasMoreElements()) {
            CommPortIdentifier com = (CommPortIdentifier) thePorts.nextElement();
            switch (com.getPortType()) {
                case CommPortIdentifier.PORT_SERIAL:
                    try {
                        CommPort thePort = com.open("CommUtil", 50);
                        thePort.close();
                        h.add(com);
                    } catch (PortInUseException e) {
                        System.out.println("Port, "  + com.getName() + ", is in use.");
                    } catch (Exception e) {
                        System.err.println("Failed to open port " + com.getName());
                        e.printStackTrace();
                    }
            }
        }
        return h;
    }

    /**
     * This method iterates through the HashSet of available serial ports en if there is match
     * it tries to open the port and assign this to the arduinoSerialPort property of ArduinoConnector object.
     * @param theAvailableSerialPorts a HashSet of CommPortIdentifiers (id of serial port)
     * @return true or false depends on success of opening the arduino port
     */
    private boolean openArduinoPort(HashSet<CommPortIdentifier> theAvailableSerialPorts) {

        for (CommPortIdentifier port: theAvailableSerialPorts) {
            for (String portName : ARUDINO_PORT_NAMES) {
                if (port.getName().equals(portName)
                        || port.getName().startsWith(portName)) {
                    // Try to connect to the Arduino on this port
                    try {
                        arduinoSerialPort = (SerialPort) port.open(this.getClass().getName(), TIME_OUT);
                        System.out.println(port.getName() + " is opened");
                        return true;
                    } catch (PortInUseException p) {
                        System.out.println(port.getName() + " is already used");
                    }
                }
            }
        }
        return false;
    }

    /**
     *This method set the parameters for serial communication like, DATA_RATE is baud rate
     * and how many bytes per time will be sent through port etc.
     */
    private void  setUpArduinoPort() {
        // set port parameters
        try {
            this.arduinoSerialPort.setSerialPortParams(
                    DATA_RATE,
                    SerialPort.DATABITS_8,
                    SerialPort.STOPBITS_1,
                    SerialPort.PARITY_NONE);

        } catch (UnsupportedCommOperationException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method will initialize the input and output streams by properties of ArduinoConnector object.
     */
    private void initializeIOstreams(){
        try {
            input = new BufferedReader(new InputStreamReader(this.arduinoSerialPort.getInputStream()));
            output = this.arduinoSerialPort.getOutputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
        // add event listeners
        try {
            this.arduinoSerialPort.addEventListener(this);
        } catch (TooManyListenersException e) {
            e.printStackTrace();
        }
        this.arduinoSerialPort.notifyOnDataAvailable(true);
        System.out.println("connection streams are set");
        System.out.println("Arduino connector is ready");
    }

    /**
     * Handle an event on the serial port. Read the data and print it.
     */
    public synchronized void serialEvent(SerialPortEvent oEvent) {
        if (oEvent.getEventType() == SerialPortEvent.DATA_AVAILABLE) {
            try {
                inputLine = input.readLine();
                System.out.println(inputLine);
            } catch (Exception e) {
                System.err.println(e.toString());
            }
        }
    }

    /**
     * Getter for inputLine property of ArduinoConnector which is (re-)assigned by serialEvent method)
     */
    public String getResponse() {
        try {
            //It has to be paused for ca 3 sec because of delay of response of arduino. Otherwise we get empty string.
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return inputLine;
    }

    /**
     * This method will write to the Arduino serial port an array of bytes made from string which is supplied as arg.
     * @param msg string you want to send to Arduino
     */
    public void write (String msg)
    {
        try {
            Thread.sleep(3000); // I has to be a paused for a while,
            // because arduino can't handle many commands after each other and the opening the Arduino port take also a
            //while which can cause that the message will not be received by Arduino
            String msgWithMarkers = "<" + msg + ">";
            try {
                //getBytes function will convert the string to array of bytes
                this.output.write(msgWithMarkers.getBytes());
                this.output.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getId()+ "-" + msg + " is sent");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
