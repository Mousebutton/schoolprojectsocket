package nl.hhs.central;

import nl.hhs.connection.ConnectionStatus;
import nl.hhs.connection.Connector;
import nl.hhs.connection.MessageFactory;
import nl.hhs.connection.MessageParser;

import java.io.IOException;

/**
 * This class and it's statics methods are used to handle the command messages from the server.
 */
public class CommandHandler {

    /**
     * This method is used for delegating the commands from msg to the responsible methods to execute the command
     * @param rawData unformatted message from the server
     * @param ac Arduino connector th write and listen to the arduino port
     * @param serverCon to send the response messages to the server
     * @return true or false if the command is executed/ not executed
     */

    public static Boolean handle(String rawData, ArduinoConnector ac, Connector serverCon)  {
        MessageParser mp = new MessageParser(rawData);

        switch (mp.getCommand()) {
            case "setCurtain":
              return setCurtain(mp, ac);
            case "getCurtain":
                return getCurtain(mp, serverCon);
            case "getSensorData":
                return getSensorData(ac, serverCon);
            //this case is for ping request from server
            case "getHey":
                return hey(serverCon);
        }
        return false;
    }

    /**
     * This method change the curtain object values.
     * @param mp MessageParser is used to format the message to filter the new args/values of the curtain
     * @param ac ArduinoConnector is used to change the position of curtain. If the position is the same
     *           arduino will do nothing
     * @return true or false if the command is executed/not executed
     */
    private static boolean setCurtain(MessageParser mp, ArduinoConnector ac){
        int curtainPercentagePosition = Integer.parseInt(mp.getArgs().get(0).get("v"));
        //Check if there is only arguments line if there are more the command is invalid
        if (mp.getArgs().size() == 1) {
            if (curtainPercentagePosition == 100) {
                ac.write("o");
                if(ac.getResponse().equals("180")){
                    JSONRegisterManager.editCurtain(mp.getArgs());
                    return true;
                }else{return false;}

            } else if (curtainPercentagePosition == 0) {
                ac.write("c");
                if(ac.getResponse().equals("0")){
                    JSONRegisterManager.editCurtain(mp.getArgs());
                    return true;
                }else{return false;}
            }
            else {
                if (Curtain.percentageToArduinoPosition(curtainPercentagePosition) != -1) {
                    ac.write("s " + Curtain.percentageToArduinoPosition(curtainPercentagePosition));
                    if (ac.getResponse().equals("" + Curtain.percentageToArduinoPosition(curtainPercentagePosition))){
                        JSONRegisterManager.editCurtain(mp.getArgs());
                        return true;
                    }else{return false;}
                } else { return false; }
            }
        } else { return false; }
    }

    /**
     * This method will find the curtain object in the HashSet with curtains. That curtain will be converted with
     * message parser to string response message for the server and send it to the server. This method is synchronized
     * because the register HashSet is shared asset with the DayAndNight thread.
     * @param mp MessageParser to convert the Curtain object to the message for the server.
     * @param serverCon to send the the curtain message to the server.
     * @return true or false if the command is executed/not executed
     */
    private static synchronized boolean getCurtain(MessageParser mp, Connector serverCon){
        int curtainId = Integer.parseInt(mp.getArgs().get(0).get("g"));
            for (Curtain c: JSONRegisterManager.decodeRegisterToHashSet()) {
                if(curtainId == c.getId()){
                    serverCon.sendData(MessageFactory.createCurtainsMessage(1,c));
                    return true;
                }else{ return false;}
            }
        return false;
    }

    /**
     * This method is used to ask arduino for sensor data of curtain and if the response is in the decent time back
     * send it to the server.
     * @param ac ArduinoConnector is used to ask sensor data of curtain.
     * @param serverCon to send the the curtain message to the server.
     * @return true or false if the command is executed/not executed
     */
    private static boolean getSensorData(ArduinoConnector ac, Connector serverCon) {
        ac.write("d");
        long startTime = System.currentTimeMillis();
        while (true) {
            String response = ac.getResponse();
            if (ac.getResponse().startsWith("d")) {
                serverCon.sendData(MessageFactory.createSensorDataMessage(1,1,response));
                return true;
            } else if (System.currentTimeMillis() - startTime >= 3000) {
                return false;
            }
        }
    }

    /**
     * This method is to ping the response if the server asks for it.
     * @param serverCon to send the ping "Hey" message to the server
     * @return true when a message is send
     */
    private static boolean hey(Connector serverCon){
        serverCon.sendData(ConnectionStatus.ISOPEN.toString());
        return true;
    }
}
