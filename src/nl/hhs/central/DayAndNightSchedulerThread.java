package nl.hhs.central;

import java.io.IOException;

import static nl.hhs.central.JSONRegisterManager.decodeRegisterToHashSet;

/**
 * This class is a thread which every 15 sec examine all curtains with property dayAndNight with true value
 * if the sensor value is above or below the day limit (breakpoint between day and night) it wil make an action
 * based on the sensor data like close curtain or open the curtain.
 */
public class DayAndNightSchedulerThread extends Thread {
    private ArduinoConnector ac;

    public DayAndNightSchedulerThread(ArduinoConnector ac){
        this.ac = ac;
    }
    @Override
    public void run() {
        try {
            while (true){
                    //decodeRegister returns HashSet with curtains
                    Curtain.examineDayAndNight(decodeRegisterToHashSet(), ac);
                    Thread.sleep(10000);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
